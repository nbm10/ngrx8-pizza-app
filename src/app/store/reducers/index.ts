import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';

import * as fromRouter from '@ngrx/router-store';
import { Params, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

/**
 * recommended to abstract a feature key string to prevent
 * hardcoding strings when registering feature state and calling createFeatureSelector.
 */
export const routerFeatureKey = 'router';

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

export interface State {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer
};
/**
 * Custom serializer should implement the RouterStateSerializer interface
 * this interface contains a serialize method that takes the RouterStateSnapshot as an argument
 * from this RouterStateSnapshot we can compose the router state that we want returned instead of returning the whole complex snapshot
 */
export class CustomSerializer implements fromRouter.RouterStateSerializer<RouterStateUrl> {

  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const { url } = routerState;
    const { queryParams } = routerState.root;

    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      state = state.firstChild;
    }

    const {params } = state;
    return { url, queryParams, params };
  }
}

export const getRouterState = createFeatureSelector<fromRouter.RouterReducerState<RouterStateUrl>>(routerFeatureKey);

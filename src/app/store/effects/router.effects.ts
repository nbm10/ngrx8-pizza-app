import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType } from '@ngrx/effects';

import { map, tap } from 'rxjs/operators';
import * as routerActions from '../actions/router.actions';

@Injectable()
export class RouterEffects {
  navigate$ = createEffect(() =>
  this.actions$.pipe(
      ofType(routerActions.Go),
      map(action => action.payload),
      tap(({ path, query: queryParams, extras }) => {
        this.router.navigate(path, { queryParams, ...extras })
      })
    ),
    { dispatch: false}
  );


  navigateBack$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerActions.Back),
      tap(() => this.location.back())
    )
  );


  navigateForward$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerActions.Forward),
      tap(() => this.location.forward())
    )
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location
  ) {}
}

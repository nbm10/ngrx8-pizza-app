import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { ProductsRoutingModule } from './products-routing.module';

import * as fromContainers from '../products/containers';
import * as fromComponents from '../products/components';
import * as fromServices from '../products/services';
import * as fromProducts from './store';
import * as fromGuards from './guards';

@NgModule({
  declarations: [...fromContainers.containers, ...fromComponents.components],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    ProductsRoutingModule,
    StoreModule.forFeature(fromProducts.productsFeatureKey, fromProducts.reducers),
    EffectsModule.forFeature(fromProducts.effects),
  ],
  providers: [...fromServices.services, ...fromGuards.guards]
})
export class ProductsModule { }

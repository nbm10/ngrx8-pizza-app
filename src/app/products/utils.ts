import { Observable } from 'rxjs';
import { Action, Store, select } from '@ngrx/store';
import { tap, filter, take } from 'rxjs/operators';

export function checkStore<T, S, A extends Action>(store: Store<T>, selector: S, action: A): Observable<boolean> {
    return store.pipe(
      select(selector),
      /**
       * tap is used to get out of the observable stream
       * to the stream, you can use the value received from
       * without affecting the stream or making changes
       * the stream and do something with it.
       */
      tap(loaded => {
      // here we check if the items have beeen loaded and if not we dispatch an action to load the items
        if (!loaded) {
          store.dispatch(action);
        }
      }),
      // we use filter to only return loaded only if loaded is completely true
      filter(loaded => loaded),
      // we use take to grab the first value of this stream and then unsubscribe automatically from the observable stream
      take(1)
    );
  }

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { select, Store } from '@ngrx/store';

import { Pizza } from '../../models/pizza.model';
import { Topping } from '../../models/topping.model';
import * as fromStore from '../../store';

@Component({
  selector: 'product-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  pizza$: Observable<Pizza>;
  visualise$: Observable<Pizza>;
  toppings$: Observable<Topping[]>;

  constructor(private store: Store<fromStore.ProductsState>) { }

  ngOnInit() {
    this.pizza$ = this.store.pipe(
      select(fromStore.selectSelectedPizza)
    ).pipe(
      tap((pizza: Pizza = null) => {
        const pizzaExists = !!(pizza && pizza.toppings);
        const toppings = pizzaExists ? pizza.toppings.map(topping => topping.id) : [];
        this.store.dispatch(fromStore.VisualiseToppings({ selectedToppings: toppings }));
      })
    );
    this.toppings$ = this.store.pipe(
      select(fromStore.selectAllToppings)
    );
    this.visualise$ = this.store.pipe(
      select(fromStore.selectPizzaVisualised)
    );
  }

  onSelect(event: number[]) {
    this.store.dispatch(fromStore.VisualiseToppings({ selectedToppings: event }));
  }

  onCreate(event: Pizza) {
    this.store.dispatch(fromStore.AddPizza({ pizza: event }));
  }

  onUpdate(event: Pizza) {
    this.store.dispatch(fromStore.UpdatePizza({ pizza: event }));
  }

  onRemove(event: Pizza) {
    const remove = window.confirm('Are you sure?');
    if (remove) {
      this.store.dispatch(fromStore.RemovePizza({ pizza: event }));
    }
  }
}

import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { catchError, concatMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

import * as toppingsActions from '../actions/toppings.actions';
import { ToppingsService } from '../../services';

@Injectable()
export class ToppingsEffect {
  loadToppings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(toppingsActions.LoadToppings),
      concatMap(() => {
        return this.toppingsService.getToppings().pipe(
          map(toppings => toppingsActions.LoadToppingsSuccess({ toppings })),
          catchError(error => of(toppingsActions.LoadToppingsfail({ error })))
        );
      })
    )
  );

  constructor(private actions$: Actions, private toppingsService: ToppingsService) {}
}

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { catchError, concatMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

import { PizzasService } from '../../services/pizzas.service';
import * as pizzasActions from '../actions';
import * as fromRoot from '../../../store';

@Injectable()
export class PizzasEffect {
  loadPizzas$ = createEffect(() =>
    this.actions$.pipe(
      ofType(pizzasActions.LoadPizzas),
      concatMap(() => this.pizzasService.getPizzas().pipe(
        map(pizzas => pizzasActions.LoadPizzasSuccess({ pizzas })),
        catchError(error => of(pizzasActions.LoadPizzasFail({ error })))
      ))
    )
  );

  addPizza$ = createEffect(() =>
    this.actions$.pipe(
      ofType(pizzasActions.AddPizza),
      map(action => action.pizza),
      concatMap(payload => {
        return this.pizzasService.createPizza(payload).pipe(
          map(pizza => pizzasActions.AddPizzaSuccess({ pizza })),
          catchError(error => of(pizzasActions.AddPizzaFail({ error })))
        );
      })
    )
  );

  addPizzaSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(pizzasActions.AddPizzaSuccess),
      map(action => action.pizza),
      map(pizza => {
        return fromRoot.Go({
          payload: {
            path: ['/products', pizza.id],
          }
        });
      })
    )
  );

  updatePizza$ = createEffect(() =>
    this.actions$.pipe(
      ofType(pizzasActions.UpdatePizza),
      map(action => action.pizza),
      concatMap(payload => {
        return this.pizzasService.updatePizza(payload).pipe(
          map((pizza) => {
            return pizzasActions.UpdatePizzaSuccess({
              pizza: {
                id: pizza.id,
                changes: pizza
              }
            });
          }),
          catchError(error => of(pizzasActions.UpdatePizzaFail({ error })))
        );
      })
    )
  );

  removePizza$ = createEffect(() =>
    this.actions$.pipe(
      ofType(pizzasActions.RemovePizza),
      map(action => action.pizza),
      concatMap(payload => {
        return this.pizzasService.removePizza(payload).pipe(
          map(() => pizzasActions.RemovePizzaSuccess({ pizza: payload })),
          catchError(error => of(pizzasActions.RemovePizzaFail({ error })))
        );
      })
    )
  );

  handlePizzaSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        pizzasActions.UpdatePizzaSuccess,
        pizzasActions.RemovePizzaSuccess),
        map(() => {
          return fromRoot.Go({
            payload: {
              path: ['/products'],
            }
          });
        })
    )
  );

  constructor(private actions$: Actions, private pizzasService: PizzasService) {}
}

import { createAction, props } from '@ngrx/store';

import { Topping } from '../../models/topping.model';

export const LoadToppings = createAction('[Products] Load Toppings');
export const LoadToppingsSuccess = createAction('[Products] Load Toppings Success', props<{ toppings: Topping[] }>());
export const LoadToppingsfail = createAction('[Products] Load Toppings fail', props<{ error: object }>());
export const VisualiseToppings = createAction('[Products] Visualise Toppings', props<{selectedToppings: number[]}>());

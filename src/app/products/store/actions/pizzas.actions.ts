import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Pizza } from '../../models/pizza.model';

// Load Pizzas Actions

export const LoadPizzas = createAction('[Products] Load Pizzas');
export const LoadPizzasSuccess = createAction('[Products] Load Pizzas Success', props<{ pizzas: Pizza[] } >());
export const LoadPizzasFail = createAction('[Pizzas] Load Pizzas Fail', props<{ error: object}>());

// Create Pizza

export const AddPizza = createAction('[Products] Add Pizza', props<{ pizza: Pizza }>());
export const AddPizzaSuccess = createAction('[Products] Add Pizza Success', props<{ pizza: Pizza }>());
export const AddPizzaFail = createAction('[Products] Add Pizza Fail', props<{ error: object }>());

// Update Pizza

export const UpdatePizza = createAction('[Products] Update Pizza', props<{ pizza: Pizza }>());
export const UpdatePizzaSuccess = createAction('[Products] Update Pizza Success', props<{ pizza: Update<Pizza> }>());
export const UpdatePizzaFail = createAction('[Products] Update Pizza Fail', props<{ error: object }>());

// Remove Pizza

export const RemovePizza = createAction('[Products] Remove Pizza', props<{ pizza: Pizza }>());
export const RemovePizzaSuccess = createAction('[Products] Remove Pizza Success', props<{ pizza: Pizza }>());
export const RemovePizzaFail = createAction('[Products] Remove Fail', props<{ error: object }>());

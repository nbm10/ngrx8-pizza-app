import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../store/reducers/index';
import * as fromProducts from '../reducers/index';
import * as fromPizzas from '../reducers/pizzas.reducer';
import * as fromToppings from './toppings.selectors';


const selectPizzaState = createSelector(fromProducts.getProductsState, fromProducts.getPizzaState);
export const selectPizzasEntities = createSelector(selectPizzaState, fromPizzas.getEntities);
export const selectAllPizzas = createSelector(selectPizzaState, fromPizzas.getPizzas);
export const selectPizzasLoaded = createSelector(selectPizzaState, fromPizzas.getLoaded);
export const selectPizzasLoading = createSelector(selectPizzaState, fromPizzas.getLoading);
export const selectPizzasError = createSelector(selectPizzaState, fromPizzas.getError);

export const selectSelectedPizza = createSelector(fromRoot.getRouterState, selectPizzasEntities, ({ state }, entities) => {
  return state && entities[state.params.pizzaId];
});

export const selectPizzaVisualised = createSelector(
  selectSelectedPizza,
  fromToppings.selectToppingsEntities,
  fromToppings.selectSelectedToppings,
  (pizza, toppingEntities, selectedToppings) => {
  const toppings = selectedToppings.map(id => toppingEntities[id]);
  return {
    ...pizza, toppings
  };
});

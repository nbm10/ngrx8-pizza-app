import { createSelector } from '@ngrx/store';

import * as fromProducts from '../../store/reducers';
import * as fromToppings from '../reducers/toppings.reducer';

const selectToppingsState = createSelector(fromProducts.getProductsState, fromProducts.getToppingsState);
export const selectToppingsEntities = createSelector(selectToppingsState, fromToppings.getToppingsEntities);
export const selectAllToppings = createSelector(selectToppingsState, fromToppings.getAllToppings);
export const selectToppingsLoaded = createSelector(selectToppingsState, fromToppings.getloaded);
export const selectToppingsLoading = createSelector(selectToppingsState, fromToppings.getloading);
export const selectToppingsError = createSelector(selectToppingsState, fromToppings.getError);
export const selectSelectedToppings = createSelector(selectToppingsState, fromToppings.getSelectedToppings);

import { createReducer, Action, on } from '@ngrx/store';
import { createEntityAdapter, EntityState, EntityAdapter } from '@ngrx/entity';

import { Topping } from '../../models/topping.model';
import * as toppingsActions from '../actions/toppings.actions';

export const toppingsFeatureKey = 'toppings';

export interface ToppingsState extends EntityState<Topping> {
  loaded: boolean;
  loading: boolean;
  error: object;
  selectedToppings: number[];
}

export const toppingsAdapter: EntityAdapter<Topping> = createEntityAdapter();

const initialState: ToppingsState = toppingsAdapter.getInitialState({
  loaded: false,
  loading: false,
  error: {},
  selectedToppings: [],
});

export const toppingsReducer = createReducer(
  initialState,
  on(toppingsActions.LoadToppings, state => ({
    ...state,
    loaded: false,
    loading: false,
  })),
  on(toppingsActions.LoadToppingsSuccess, (state, { toppings }) => {
    return toppingsAdapter.addAll(toppings, {
      ...state,
      loaded: true,
      loading: false,
    });
  }),
  on(toppingsActions.LoadToppingsfail, (state, { error }) => ({
    ...state,
    loaded: false,
    loading: false,
    error,
  })),
  on(toppingsActions.VisualiseToppings, (state, { selectedToppings }) => ({
    ...state,
    selectedToppings
  }))
);

export function reducer(state: ToppingsState | undefined, action: Action) {
  return toppingsReducer(state, action);
}

const {
  selectEntities,
  selectAll
} = toppingsAdapter.getSelectors();

export const getToppingsEntities = selectEntities;
export const getAllToppings = selectAll;
export const getloaded = (state: ToppingsState) => state.loaded;
export const getloading = (state: ToppingsState) => state.loading;
export const getError = (state: ToppingsState) => state.error;
export const getSelectedToppings = (state: ToppingsState) => state.selectedToppings;

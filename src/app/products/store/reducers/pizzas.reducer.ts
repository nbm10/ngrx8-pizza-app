import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Pizza } from '../../models/pizza.model';
import * as pizzaActions from '../actions/pizzas.actions';

export const pizzasFeatureKey = 'pizzas';

export interface PizzaState extends EntityState<Pizza> {
  loaded: boolean;
  loading: boolean;
  error: object;
}

export const pizzaAdapter: EntityAdapter<Pizza> = createEntityAdapter<Pizza>();

export const initialState: PizzaState = pizzaAdapter.getInitialState({
  loaded: false,
  loading: false,
  error: {},
});

export const pizzaReducer = createReducer(
  initialState,
  on(pizzaActions.LoadPizzas, (state) => ({
    ...state,
    loaded: false,
    loading: true,
  })),
  on(pizzaActions.LoadPizzasSuccess, (state, { pizzas }) => {
    return pizzaAdapter.addAll(pizzas, {
      ...state,
      loaded: true,
      loading: false,
    });
  }),
  on(pizzaActions.AddPizzaSuccess, (state, { pizza }) => {
    return pizzaAdapter.addOne(pizza, state);
  }),
  on(pizzaActions.UpdatePizzaSuccess, (state, { pizza }) => {
    return pizzaAdapter.updateOne(pizza, state);
  }),
  on(pizzaActions.RemovePizzaSuccess, (state, { pizza }) => {
    return pizzaAdapter.removeOne(pizza.id, state);
  }),
  on(
    pizzaActions.LoadPizzasFail,
    pizzaActions.AddPizzaFail,
    pizzaActions.UpdatePizzaFail,
    pizzaActions.RemovePizzaFail,
    (state, { error }) => ({
    ...state,
    loaded: false,
    loading: false,
    error
  })),
);

export function reducer(state: PizzaState | undefined, action: Action) {
  return pizzaReducer(state, action);
}

const {
  selectEntities,
  selectAll
} = pizzaAdapter.getSelectors();

export const getEntities = selectEntities;
export const getPizzas = selectAll;
export const getLoaded = (state: PizzaState) => state.loaded;
export const getLoading = (state: PizzaState) => state.loading;
export const getError = (state: PizzaState) => state.error;


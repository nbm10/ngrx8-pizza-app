import { createFeatureSelector, combineReducers, Action } from '@ngrx/store';

import * as fromRoot from '../../../store/reducers';
import * as fromPizzas from './pizzas.reducer';
import * as fromToppings from './toppings.reducer';

export const productsFeatureKey = 'products';

export interface ProductsState {
  [fromPizzas.pizzasFeatureKey]: fromPizzas.PizzaState;
  [fromToppings.toppingsFeatureKey]: fromToppings.ToppingsState;
}

// TODO: find out what this does
export interface State extends fromRoot.State {
  [productsFeatureKey]: ProductsState;
}

export function reducers(state: ProductsState, action: Action) {
  return combineReducers({
    [fromPizzas.pizzasFeatureKey]: fromPizzas.reducer,
    [fromToppings.toppingsFeatureKey]: fromToppings.reducer,
  })(state, action);
}

export const getPizzaState = (state: ProductsState) => state.pizzas;
export const getToppingsState = (state: ProductsState) => state.toppings;
export const getProductsState = createFeatureSelector<ProductsState>(productsFeatureKey);
